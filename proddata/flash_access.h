/*
 * Copyright (C) 2016 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @file
 * Abstract class for flash access
 *
 * @author Imagination Technologies
 *
 * @copyright <b>Copyright 2016 by Imagination Technologies Limited and/or its affiliated group companies.</b>
 */

#ifndef FLASHACCESS_H_
#define FLASHACCESS_H_

#include <string>
#include <vector>

/**
 * @brief Abstract class for flash access
 */
class FlashAccess {
 public:
  explicit FlashAccess(const std::string &device_name);
  virtual ~FlashAccess();

  /**
   * @brief Write data to flash
   *
   * @param[in] buf chunk of data to be written
   * @param[in] offset device offset
   */
  virtual void Write(const std::vector<uint8_t> &buf, const int offset) = 0;

  /**
   * @brief Read data from flash storage

   * @param[in] size size of data to be read
   * @param[in] offset device offset
   * @returns vector containing read data
   *
   */
  virtual std::vector<uint8_t> Read(const int size, const int offset) = 0;

  /**
   * @brief Read serial number
   *
   * returns vector containing serial number
   */
  virtual std::vector<uint8_t> ReadSerial();

 protected:
  int fd_;
};

#endif  // FLASHACCESS_H_
