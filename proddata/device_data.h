/*
 * Copyright (C) 2016 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @file
 * DeviceData class
 *
 * @author Imagination Technologies
 *
 * @copyright <b>Copyright 2016 by Imagination Technologies Limited and/or its affiliated group companies.</b>
 */


#ifndef DEVICEDATA_H_
#define DEVICEDATA_H_

#include <map>
#include <memory>
#include <string>
#include <vector>
#include "flash_access.h"

/**
 * @brief class for maintaining device data layout and performing read/write operations
 */
class DeviceData {
 public:
  /**
   * @brief Constructor
   *
   * Creates an instance of DeviceData
   * @param[in] flash_access unique pointer to FlashAccess implementation
   *
   */
  explicit DeviceData(std::unique_ptr<FlashAccess> flash_access);
  ~DeviceData();

  /**
   * @brief Parse the data into wifi calibration data and MAC and write appropriately
   *
   * @param[in] data  chunk of data to be written
   */
  void Write(const std::vector<uint8_t> &data);

  /**
   * @brief Read device data from memory
   *
   * returns vector containing raw data
   */
  std::vector<uint8_t> Read();

  /**
   * @brief Read device data value e.g MAC_0
   *
   * @param[in] name name of data field
   * returns vector containing raw data
   */
  std::vector<uint8_t> ReadValue(const std::string &name);

  /**
   * @brief struct for storing size and offset for each field
  */
  struct DataField {
    int size;
    int offset;
  };

 private:
  const std::string key_serial{"SERIAL"};
  const std::map<std::string, struct DataField> register0_data_fields_ {
    {"CRC", {2, 0}},
    {"VERSION", {1, 2}},
    {"MAC_0", {6, 3}},
    {"MAC_1", {6, 9}},
    {"MAC_2", {6, 15}},
    {"MAC_3", {6, 21}},
    {"MAC_4", {6, 27}},
    {"MAC_5", {6, 33}},
  };
  const std::map<std::string, struct DataField> register1_data_fields_ {
    {"CRC", {2, 256}},
    {"VERSION", {1, 258}},
    {"DCXO", {1, 259}},
    {"DSSS_TX_POWER_2.4", {1, 260}},
    {"OFDM_TX_POWER_2.4_MCS7", {1, 261}},
    {"OFDM_TX_POWER_5B1_MCS5", {1, 262}},
    {"OFDM_TX_POWER_5B2_MCS5", {1, 263}},
    {"OFDM_TX_POWER_5B3_MCS5", {1, 264}},
    {"OFDM_TX_POWER_5B4_MCS5", {1, 265}},
    {"OFDM_TX_POWER_5B1_MCS7", {1, 266}},
    {"OFDM_TX_POWER_5B2_MCS7", {1, 267}},
    {"OFDM_TX_POWER_5B3_MCS7", {1, 268}},
    {"OFDM_TX_POWER_5B4_MCS7", {1, 269}},
    {"OFDM_TX_POWER_5B1_MCS8", {1, 270}},
    {"OFDM_TX_POWER_5B2_MCS8", {1, 271}},
    {"OFDM_TX_POWER_5B3_MCS8", {1, 272}},
    {"OFDM_TX_POWER_5B4_MCS8", {1, 273}},
    {"OFDM_TX_POWER_5B1_MCS9", {1, 274}},
    {"OFDM_TX_POWER_5B2_MCS9", {1, 275}},
    {"OFDM_TX_POWER_5B3_MCS9", {1, 276}},
    {"OFDM_TX_POWER_5B4_MCS9", {1, 277}},
  };

  enum RegisterName {
    register0,
    register1,
  };

  std::unique_ptr<FlashAccess> flash_access_;

  /**
   * @brief Parse data vector to mac data and wifi data and validate CRC
   *
   * @param[in] data vector containing raw data
   * @param[out] mac_str pointer to vector storing mac data
   * @param[out] wifi_str pointer to vector storing wifi data
   */
  void ParseData(const std::vector<uint8_t> &data, std::vector<uint8_t> *mac_data,
                 std::vector<uint8_t> *wifi_data);

  /**
   * @brief Get Sum of size of all data fields in map
   *
   * @param[in] register_name enum specifying which register map to use
   */
  int GetRegisterSize(RegisterName register_name);

  /**
   * @brief Get offset of CRC field from register map
   *
   * @param[in] register_name enum specifying which register map to use
   */
  int GetCRCOffset(RegisterName register_name);

  /**
   * @brief Get DataField structure(size, offset) from corresponding name(key)
   *
   * @param[in] register_name enum specifying which register map to use
   * @param[in] name  name of data field
   */
  const DeviceData::DataField GetDataField(RegisterName register_name, const std::string &name);
};

#endif  // DEVICEDATA_H_
