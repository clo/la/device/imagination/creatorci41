/*
 * Copyright (C) 2016 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @file
 * MTDAccess class
 *
 * @author Imagination Technologies
 *
 * @copyright <b>Copyright 2016 by Imagination Technologies Limited and/or its affiliated group companies.</b>
 */

#ifndef MTDACCESS__H_
#define MTDACCESS__H_

#include <mtd/mtd-user.h>
#include <string>
#include <vector>
#include "flash_access.h"

/**
 * @brief MTDAccess class to perfrom read/write on mtd device
 */
class MTDAccess final: public FlashAccess {
 public:
  /**
   * @brief Constructor
   *
   * Creates an instance of MTDAccess
   *
   */
  explicit MTDAccess(const std::string &device_name);
  ~MTDAccess();

  void Write(const std::vector<uint8_t> &buf, const int offset);
  std::vector<uint8_t> Read(const int size, const int offset);

 private:
  mtd_info_t mtd_info_;
};

#endif  // MTDACCESS_H_
