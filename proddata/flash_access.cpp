/*
 * Copyright (C) 2016 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @file
 * Abstract class for flash access
 *
 * @author Imagination Technologies
 *
 * @copyright <b>Copyright 2016 by Imagination Technologies Limited and/or its affiliated group companies.</b>
 */

#include "flash_access.h"
#ifdef __ANDROID__
#include <android-base/logging.h>
#define DLOG(x) LOG(x)
#else
#include <glog/logging.h>
#endif
#include <mtd/mtd-user.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <stdexcept>

FlashAccess::FlashAccess(const std::string &device_name) {
  fd_ = open(device_name.c_str(), O_RDWR);
  if (fd_ < 0) {
    DLOG(ERROR) << "Can't open device: " << strerror(errno);
#ifdef __ANDROID__
    exit(-1);
#else
    throw std::runtime_error("FlashAccess Initialization failed");
#endif
  }
}

FlashAccess::~FlashAccess() {
  close(fd_);
}

std::vector<uint8_t> FlashAccess::ReadSerial() {
  DLOG(INFO) << "Reading serial number";
  const int size = 8;
  std::vector<uint8_t> buf(size);
  int val = MTD_OTP_FACTORY;
  if (ioctl(fd_, OTPSELECT, &val) < 0) {
    DLOG(ERROR) << "ioctl failed and returned error: " << strerror(errno);
#ifdef __ANDROID__
    exit(-1);
#else
    throw std::runtime_error("Factory OTP access failed");
#endif
  }

  if (lseek(fd_, 0, SEEK_SET) < 0) {
    DLOG(ERROR) << "read serial: lseek failed: " << strerror(errno);
#ifdef __ANDROID__
    exit(-1);
#else
    throw std::runtime_error("read serial: lseek failed");
#endif
  }

  int ret = read(fd_, buf.data(), buf.size());
  if (ret < 0) {
    DLOG(ERROR) << "read serial num failed:" << strerror(errno);
#ifdef __ANDROID__
    exit(-1);
#else
    throw std::runtime_error("read serial num failed");
#endif
  }
  return buf;
}

