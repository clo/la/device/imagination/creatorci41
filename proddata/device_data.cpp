/*
 * Copyright (C) 2016 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @file
 * DeviceData class
 *
 * @author Imagination Technologies
 *
 * @copyright <b>Copyright 2016 by Imagination Technologies Limited and/or its affiliated group companies.</b>
 */

#include "device_data.h"
#ifdef __ANDROID__
#include <android-base/logging.h>
#define DLOG(x) LOG(x)
#else
#include <glog/logging.h>
#endif


static const int kCRCSize = 2;

/* CRC-16 lookup table */
static const uint16_t crctab16[] = {
  0x0000,  0xc0c1,  0xc181,  0x0140,  0xc301,  0x03c0,  0x0280,  0xc241,
  0xc601,  0x06c0,  0x0780,  0xc741,  0x0500,  0xc5c1,  0xc481,  0x0440,
  0xcc01,  0x0cc0,  0x0d80,  0xcd41,  0x0f00,  0xcfc1,  0xce81,  0x0e40,
  0x0a00,  0xcac1,  0xcb81,  0x0b40,  0xc901,  0x09c0,  0x0880,  0xc841,
  0xd801,  0x18c0,  0x1980,  0xd941,  0x1b00,  0xdbc1,  0xda81,  0x1a40,
  0x1e00,  0xdec1,  0xdf81,  0x1f40,  0xdd01,  0x1dc0,  0x1c80,  0xdc41,
  0x1400,  0xd4c1,  0xd581,  0x1540,  0xd701,  0x17c0,  0x1680,  0xd641,
  0xd201,  0x12c0,  0x1380,  0xd341,  0x1100,  0xd1c1,  0xd081,  0x1040,
  0xf001,  0x30c0,  0x3180,  0xf141,  0x3300,  0xf3c1,  0xf281,  0x3240,
  0x3600,  0xf6c1,  0xf781,  0x3740,  0xf501,  0x35c0,  0x3480,  0xf441,
  0x3c00,  0xfcc1,  0xfd81,  0x3d40,  0xff01,  0x3fc0,  0x3e80,  0xfe41,
  0xfa01,  0x3ac0,  0x3b80,  0xfb41,  0x3900,  0xf9c1,  0xf881,  0x3840,
  0x2800,  0xe8c1,  0xe981,  0x2940,  0xeb01,  0x2bc0,  0x2a80,  0xea41,
  0xee01,  0x2ec0,  0x2f80,  0xef41,  0x2d00,  0xedc1,  0xec81,  0x2c40,
  0xe401,  0x24c0,  0x2580,  0xe541,  0x2700,  0xe7c1,  0xe681,  0x2640,
  0x2200,  0xe2c1,  0xe381,  0x2340,  0xe101,  0x21c0,  0x2080,  0xe041,
  0xa001,  0x60c0,  0x6180,  0xa141,  0x6300,  0xa3c1,  0xa281,  0x6240,
  0x6600,  0xa6c1,  0xa781,  0x6740,  0xa501,  0x65c0,  0x6480,  0xa441,
  0x6c00,  0xacc1,  0xad81,  0x6d40,  0xaf01,  0x6fc0,  0x6e80,  0xae41,
  0xaa01,  0x6ac0,  0x6b80,  0xab41,  0x6900,  0xa9c1,  0xa881,  0x6840,
  0x7800,  0xb8c1,  0xb981,  0x7940,  0xbb01,  0x7bc0,  0x7a80,  0xba41,
  0xbe01,  0x7ec0,  0x7f80,  0xbf41,  0x7d00,  0xbdc1,  0xbc81,  0x7c40,
  0xb401,  0x74c0,  0x7580,  0xb541,  0x7700,  0xb7c1,  0xb681,  0x7640,
  0x7200,  0xb2c1,  0xb381,  0x7340,  0xb101,  0x71c0,  0x7080,  0xb041,
  0x5000,  0x90c1,  0x9181,  0x5140,  0x9301,  0x53c0,  0x5280,  0x9241,
  0x9601,  0x56c0,  0x5780,  0x9741,  0x5500,  0x95c1,  0x9481,  0x5440,
  0x9c01,  0x5cc0,  0x5d80,  0x9d41,  0x5f00,  0x9fc1,  0x9e81,  0x5e40,
  0x5a00,  0x9ac1,  0x9b81,  0x5b40,  0x9901,  0x59c0,  0x5880,  0x9841,
  0x8801,  0x48c0,  0x4980,  0x8941,  0x4b00,  0x8bc1,  0x8a81,  0x4a40,
  0x4e00,  0x8ec1,  0x8f81,  0x4f40,  0x8d01,  0x4dc0,  0x4c80,  0x8c41,
  0x4400,  0x84c1,  0x8581,  0x4540,  0x8701,  0x47c0,  0x4680,  0x8641,
  0x8201,  0x42c0,  0x4380,  0x8341,  0x4100,  0x81c1,  0x8081,  0x4040
};

uint16_t update_crc16(uint16_t crc, uint8_t c) {

  crc = (crc >> 8) ^ crctab16[(crc ^ c) & 0xff];

  return crc;
}

static std::vector<uint8_t> CalculateDataCRC(const std::vector<uint8_t> &data) {
  uint16_t crc16 = 0;
  const uint8_t *ptr = data.data();
  int size = data.size();
  while (size) {
    crc16 = update_crc16(crc16, *ptr);
    ptr++;
    size--;
  }
  /* convert uint16_t crc to crc vector */
  uint8_t crc_0 = (crc16 >> 8) & 0xff;
  uint8_t crc_1 = crc16 & 0xff;

  std::vector<uint8_t> crc;

  crc.push_back(crc_0);
  crc.push_back(crc_1);

  return crc;
}

static void AddDataCRC(std::vector<uint8_t> *data) {
  std::vector<uint8_t> crc = CalculateDataCRC(*data);
  data->insert(data->begin(), crc.begin(), crc.end());
}

static void CheckDataCRC(const std::vector<uint8_t> &data) {
  /* ignore first 2 bytes which stores crc */
  std::vector<uint8_t> data_without_crc(data.begin() + kCRCSize, data.end());
  std::vector<uint8_t> crc = CalculateDataCRC(data_without_crc);

  if ((crc[0] != data[0]) || (crc[1] != data[1])) {
    LOG(ERROR) << "Data corrupted:CRC failed";
#ifdef __ANDROID__
    exit(-1);
#else
    throw std::runtime_error("Data corrupted:CRC failed");
#endif
  }
}

/* TODO: Scenario of version check will be different when more than one version is actually
  supported, need to select register map according to version read and proceed for reading
  content. */

static void CheckDataLayoutVersion(std::vector<uint8_t> &data) {
  uint8_t version = data[2];

  /* TODO: Change hardcoded version, when support for more than one version is added */
  if (version != 0x01) {
#ifdef __ANDROID__
    LOG(ERROR) << "Invalid data layout version";
    exit(-1);
#else
    throw std::runtime_error("Invalid data layout version");
#endif
  }
}

DeviceData::DeviceData(std::unique_ptr<FlashAccess> flash_access) :
    flash_access_(std::move(flash_access)) {
  DLOG(INFO) << "Initialising DeviceData";
}

DeviceData::~DeviceData() {
  DLOG(INFO) << "Deinitialising DeviceData";
}

int DeviceData::GetRegisterSize(RegisterName register_name) {
  std::map<std::string, struct DataField> register_data_fields;

  if (register_name == register0) {
    register_data_fields = register0_data_fields_;
  } else if (register_name == register1) {
    register_data_fields = register1_data_fields_;
  }

  int size = 0;
  for (const auto &field : register_data_fields) {
    size = size + field.second.size;
  }

  return size;
}

int DeviceData::GetCRCOffset(RegisterName register_name) {
  const DataField field = GetDataField(register_name, "CRC");

  return field.offset;
}

const DeviceData::DataField DeviceData::GetDataField(RegisterName register_name,
                                                     const std::string &name) {
  std::map<std::string, struct DataField> register_data_fields;

  if (register_name == register0) {
    register_data_fields = register0_data_fields_;
  } else if (register_name == register1) {
    register_data_fields = register1_data_fields_;
  }

  for (const auto &field : register_data_fields) {
    if (field.first == name) {
      return field.second;
    }
  }

  std::string error = "Invalid data field: " + name;
#ifdef __ANDROID__
  LOG(ERROR) << error;
  exit(-1);
#else
  throw std::runtime_error(error);
#endif
}

void DeviceData::ParseData(const std::vector<uint8_t> &data, std::vector<uint8_t> *mac_data,
                           std::vector<uint8_t> *wifi_data) {
  int mac_data_size = GetRegisterSize(register0) - kCRCSize;
  int wifi_data_size = GetRegisterSize(register1) - kCRCSize;
  int size = data.size();
  if (size != (mac_data_size + wifi_data_size)) {
    LOG(ERROR) << "Data size error";
#ifdef __ANDROID__
    exit(-1);
#else
    throw std::runtime_error("Data size error");
#endif
  }

  mac_data->assign(data.begin(), data.begin() + mac_data_size);
  wifi_data->assign(data.begin() + mac_data_size, data.end());

  /* Add CRC */
  AddDataCRC(mac_data);
  AddDataCRC(wifi_data);
}

void DeviceData::Write(const std::vector<uint8_t> &data) {
  std::vector<uint8_t> mac_data;
  std::vector<uint8_t> wifi_data;

  ParseData(data, &mac_data, &wifi_data);

  flash_access_->Write(mac_data, GetCRCOffset(register0));
  flash_access_->Write(wifi_data, GetCRCOffset(register1));
}

std::vector<uint8_t> DeviceData::Read() {
  std::vector<uint8_t> data[2];

  /* read data from 2 registers */
  for (int i = 0; i < 2; i++) {
    DeviceData::RegisterName register_name = static_cast<DeviceData::RegisterName>(i);
    data[i] = flash_access_->Read(GetRegisterSize(register_name), GetCRCOffset(register_name));

    /* check crc */
    CheckDataCRC(data[i]);

    /* Check version */
    CheckDataLayoutVersion(data[i]);

    /* remove crc from data */
    data[i].erase(data[i].begin(), data[i].begin() + kCRCSize);
  }

  std::vector<uint8_t> buf = data[0];
  buf.insert(buf.end(), data[1].begin(), data[1].end());

  return buf;
}

std::vector<uint8_t> DeviceData::ReadValue(const std::string &name) {
  DeviceData::RegisterName register_name;
  DataField field;
  if (register0_data_fields_.end() != register0_data_fields_.find(name)) {
    field = GetDataField(register0, name);
    register_name = register0;
  } else if (register1_data_fields_.end() != register1_data_fields_.find(name)) {
    field = GetDataField(register1, name);
    register_name = register1;
  } else if (key_serial == name) {
    return flash_access_->ReadSerial();
  } else {
    LOG(ERROR) << "Invalid data field: " << name;
#ifdef __ANDROID__
    exit(-1);
#else
    throw std::runtime_error("Invalid data field: " + name);
#endif
  }

  std::vector<uint8_t> buf;

  /* read complete register into buf to check for crc and version */
  buf = flash_access_->Read(GetRegisterSize(register_name), GetCRCOffset(register_name));
  CheckDataCRC(buf);
  CheckDataLayoutVersion(buf);

  int data_field_position = field.offset - GetCRCOffset(register_name);

  /* Get data field from buf */
  std::vector<uint8_t> data(buf.begin() + data_field_position,
                            buf.begin() + data_field_position + field.size);

  return data;
}
